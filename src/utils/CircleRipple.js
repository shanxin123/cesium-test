export const addCircleRipple = function(data) {
    let r1 = data.minR;
    const viewer = window.viewer;
    function changeR1() {
        r1 = r1 + data.deviationR
        if(r1 >= data.maxR){
            r1 = data.minR
        }
        return r1;
    }
    function getR1 () {
        return r1
    }
    viewer.entities.add({
        name: "",
        id: data.id,
        position: Cesium.Cartesian3.fromDegrees(data.lon,data.lat,data.height),
        ellipse: {
            semiMajorAxis: new Cesium.CallbackProperty(changeR1,false),
            semiMinorAxis: new Cesium.CallbackProperty(getR1,false),
            height: data.height,
            material: new Cesium.ImageMaterialProperty({
                image: data.image,
                repeat: new Cesium.Cartesian2(1.0, 1.0),
                transparent: true,
                color: new Cesium.CallbackProperty(function () {
                    return Cesium.Color.WHITE.withAlpha(1-r1/data.maxR)  //entity的颜色透明 并不影响材质，并且 entity也会透明哦
                },false)
            })
        }
    })
};
