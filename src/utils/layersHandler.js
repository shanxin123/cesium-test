export const addAdditionalLayerOption = (name, imageryProvider)=> {
    let layer =  window.viewer.imageryLayers.addImageryProvider(imageryProvider);
    // layer.alpha = Cesium.defaultValue(alpha, 0.5);
    layer.name = name;
    Cesium.knockout.track(layer, ["alpha", "show", "name"]);
}
