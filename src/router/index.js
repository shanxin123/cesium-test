import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        children: [
            {
                path: '/about',
                name: 'About',
                // route level code-splitting
                // this generates a separate chunk (about.[hash].js) for this route
                // which is lazy-loaded when the route is visited.
                component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
            },
            //基础
            {
                path: "/viewer",
                name: "viewer",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumViewer')
            },
            //相机
            {
                path: "/setView",
                name: "setView",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/Camera/CameraSetView')
            },
            {
                path: "/flyTo",
                name: "flyTo",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/Camera/CameraFlyTo')
            },
            {
                path: "/CameraCircumferentialFlight",
                name: "CameraCircumferentialFlight",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/Camera/CameraCircumferentialFlight')
            },
            //影像
            {
                path: "/CesiumIonAssetsImageryProviderExample",
                name: "CesiumIonAssetsImageryProviderExample",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/ImageryProvider/CesiumIonAssetsImageryProviderExample')
            },
            {
                path: "/ArcGisMapServerImageryProvider",
                name: "ArcGisMapServerImageryProvider",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/ImageryProvider/ArcGisMapServerImageryProvider')
            },
            {
                path: "/OpenStreetMapImageryProvider",
                name: "OpenStreetMapImageryProvider",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/ImageryProvider/OpenStreetMapImageryProvider')
            },
            {
                path: "/UrlTemplateImageryProvider",
                name: "UrlTemplateImageryProvider",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/ImageryProvider/UrlTemplateImageryProvider')
            },
            //实体
            {
                path: "/EntityBillboardLocationMark",
                name: "EntityBillboardLocationMark",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/Entity/EntityBillboardLocationMark')
            },
            {
                path: "/EntityBox",
                name: "EntityBox",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/Entity/EntityBox')
            },
            {
                path: "/EntityMove",
                name: "EntityMove",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/Entity/EntityMove')
            },
            {
                path: "/ViewScreen",
                name: "ViewScreen",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/Entity/ViewScreen')
            },
            //primitive图元
            {
                path: "/PrimitivesAppearanceMaterialWithImage",
                name: "PrimitivesAppearanceMaterialWithImage",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/Primitive/PrimitivesAppearanceMaterialWithImage')
            },
            //标绘
            {
                path: "/addBillboardLocationMarks",
                name: "addBillboardLocationMarks",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/Plot/AddBillboardLocationMarks')
            },
            {
                path: "/PlotPointsExample",
                name: "PlotPointsExample",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/Plot/PlotPointsExample')
            },
            {
                path: "/PlotLinesExample",
                name: "PlotLinesExample",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/Plot/PlotLinesExample')
            },
            {
                path: "/PlotPolygonsExample",
                name: "PlotPolygonsExample",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/Plot/PlotPolygonsExample')
            },
            //动画效果
            {
                path: "/RadarScan",
                name: "RadarScan",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/AnimationEffect/RadarScan')
            },
            {
                path: "/CircleRipple",
                name: "CircleRipple",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/AnimationEffect/CircleRipple')
            },
            {
                path: "/DoubleCircleRipple",
                name: "DoubleCircleRipple",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/AnimationEffect/DoubleCircleRipple')
            },
            {
                path: "/FlashingEntity",
                name: "FlashingEntity",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/AnimationEffect/FlashingEntity')
            },
            //模型和数据
            {
                path: "/3dTilesNewYorkCity3dBuildings",
                name: "3dTilesNewYorkCity3dBuildings",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/ModelsAndData/3dTilesNewYorkCity3dBuildings')
            },
            //自定义范例
            {
                path: "/customInfoBoxExample",
                name: "customInfoBoxExample",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/CustomExample/CustomInfoBoxExample')
            },
            {
                path: "/imageLayerSplitExample",
                name: "imageLayerSplitExample",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/CustomExample/ImageLayerSplitExample')
            },
            {
                path: "/abcControllerExample",
                name: "abcControllerExample",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/CustomExample/abcControllerExample')
            },
            {
                path: "/MeasuringScaleExample",
                name: "MeasuringScaleExample",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/CustomExample/MeasuringScaleExample')
            },
            {
                path: "/MouseFocusCoordinateExample",
                name: "MouseFocusCoordinateExample",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/CustomExample/MouseFocusCoordinateExample')
            },
            //范例二
            {
                path: "/GlobeInterior",
                name: "GlobeInterior",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/CustomExample2/GlobeInterior')
            },
            {
                path: "/ParticleSystemWeather",
                name: "ParticleSystemWeather",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/CustomExample2/ParticleSystemWeather')
            },
            {
                path: "/ParticleSystem",
                name: "ParticleSystem",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/CustomExample2/ParticleSystem')
            },
            //高德
            {
                path: "/gaodeSearchExample",
                name: "gaodeSearchExample",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/gaode/gaodeSearchExample')
            },
            {
                path: "/gaodePoiSearchExample",
                name: "gaodePoiSearchExample",
                component: () => import(/* webpackChunkName: "about" */ '../components/CesiumExample/gaode/gaodePoiSearchExample')
            },
        ]
    },
    //404页面路由，请保持在最下方
    {
        path: "/404",
        name: "notFound",
        component: () => import('../views/404')
    },
    {
        path: '*', // 404 页面
        redirect: "/404"
    }
];

const router = new VueRouter({
    routes
});

export default router
