import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'cesium/Widgets/widgets.css'
import highlightJs from 'highlight.js'
import 'highlight.js/styles/androidstudio.css';
import router from './router'
Vue.config.productionTip = false;
Vue.use(ElementUI);
Vue.directive('highlight',function (el) {
  highlightJs.configure({useBR: true});
  let blocks = el.querySelectorAll('pre code');
  blocks.forEach((block) => {
    highlightJs.highlightBlock(block)
  })
});
new Vue({
  router,
  render: h => h(App)
}).$mount('#app');
